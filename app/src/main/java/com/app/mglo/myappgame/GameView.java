package com.app.mglo.myappgame;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.app.mglo.myappgame.characters.SpriteObject;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private SpriteObject sprite;
    private GameLogic gameLogic;


    public GameView(Context context) {
        super(context);
        sprite = new SpriteObject(BitmapFactory.decodeResource(getResources(), R.drawable.green), 50, 50);
        gameLogic = new GameLogic("GameThread", getHolder(), this);
        getHolder().addCallback(this);
        setFocusable(true);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        gameLogic.setGame_state(GameLogic.RUNNING);
        gameLogic.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawColor(Color.BLACK);
        sprite.draw(canvas);
    }

    public void update(){
        sprite.update();
    }
}
