package com.app.mglo.myappgame.characters;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class SpriteObject {
    private Bitmap bitmap;
    private int x;
    private int y;
    private int x_move = 5;
    private int y_move = 5;


    public SpriteObject(Bitmap bitmap, int x, int y) {
        this.bitmap = bitmap;
        this.x = x;
        this.y = y;
    }

    public void draw(Canvas canvas){
        canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() /2), null);
    }

    public void update(){
        x += ( x_move);
        y += ( y_move);
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}
