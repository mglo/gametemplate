package com.app.mglo.myappgame;

import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.view.SurfaceHolder;

public class GameLogic extends Thread {

    private SurfaceHolder surfaceHolder;
    private GameView gameView;
    private int game_state;
    public static final int PAUSE = 0;
    public static final int READY = 1;
    public static final int RUNNING = 2;

    public GameLogic(@NonNull String name, SurfaceHolder surfaceHolder, GameView gameView) {
        super(name);
        this.surfaceHolder = surfaceHolder;
        this.gameView = gameView;
    }

    public int getGame_state() {
        return game_state;
    }

    public void setGame_state(int game_state) {
        this.game_state = game_state;
    }

    @Override
    public void run() {
        Canvas canvas;
        while(game_state == RUNNING){
            canvas = null;
            long time_orig = System.currentTimeMillis();
            try{
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (surfaceHolder){
                    this.gameView.update();
                    this.gameView.draw(canvas);

//                    long time_interim = System.currentTimeMillis();
//                    int adj_mov = (int)(time_interim - time_orig);
//
//                    gameView.update(adj_mov);
//                    time_orig = time_interim;
//                    gameView.draw(canvas);
                }
            }finally {
                if(canvas != null){
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }
}
